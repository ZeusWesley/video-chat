<!-- ERROR MESSAGES[INICIO] -->
@if(isset($errors))
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endif
<!-- ERROR MESSAGES[FIM] -->

@if(session('viewException'))
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        @if(is_string(session('viewException')))
            {!! session('viewException') !!}
        @endif
    </div>
@endif

@if(session('iuguException'))
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>

        @if(is_array(session('iuguException')))
            @foreach(session('iuguException') as $field => $bag)
                <li><b>{{ strtoupper($field) }}</b></li>
                <ul>
                    @foreach($bag as $key => $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            @endforeach

        @else
            {{ session('iuguException') }}
        @endif
    </div>
@endif

@if(session('viewExceptionBag'))
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach(session('viewExceptionBag')['errors'] as $type => $bag)
                <li><b>{{ strtoupper($type) }}</b></li>
                <ul>
                    @foreach($bag as $key => $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            @endforeach
        </ul>
    </div>
@endif



<!-- ALERT MESSAGES [INICIO] -->
@if(session('alert'))
    <div class="alert alert-{{ session('alert')['type'] }} alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {!! session('alert')['message'] !!}
    </div>
@endif
<!-- ALERT MESSAGES [FIM] -->

<!-- SUCCESS MESSAGES [INICIO] -->
@if(session('success'))
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ session('success') }}
    </div>
@endif
<!-- SUCCESS MESSAGES [FIM] -->

<!-- ERROR MESSAGES [INICIO] -->
@if(session('error-message'))
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ session('error-message') }}
    </div>
@endif
<!-- ERROR MESSAGES [FIM] -->
