<div class="bg-light text-center px-3 py-3">
    <a class="menu-item" href="{{ url('/') }}">
        <img src="{{ asset('assets/img/AWRlogo.png') }}" alt="All Woman Right" class="sidebar-logo">
    </a>
</div>

<div class="p-3">
    <ul class="menu">
        <li class="item">
            <a class="menu-item" href="{{ url('/') }}">
                <i class="material-icons">home</i>
                <span>Home</span></a>
        </li>

        @authType(admin)
            <li class="item">
                <a class="menu-item" href="{{ url('users') }}">
                    <i class="material-icons">people</i>
                    <span>Gestão de usuários</span></a>
            </li>
        @endAuthType

        <li class="item">
            <a class="menu-item" href="{{ url('users/edit/'. \Illuminate\Support\Facades\Auth::user()->id) }}">
                <i class="material-icons">fingerprint</i>
                <span>Minha conta</span></a>
        </li>

{{--        <li class="item">--}}
{{--            <a class=menu-item href="{{ url('clients') }}">--}}
{{--                <i class="material-icons">person_outline</i>--}}
{{--                Cliente</a>--}}
{{--        </li>--}}

        <li class="item">
            <a class="menu-item" href="{{ url('messages') }}">
                <i class="material-icons">ondemand_video</i>
                <span>Material</span></a>
        </li>

    </ul>
</div>
