<nav aria-label="...">
    <div>{{ $data->links() }}</div>
    @if($data->count() > 0 )
        <small class="text-muted">
            Listando <strong>{{ $data->count() }}</strong>
            {{ $data->count() > 1 ? 'registros' : 'registro' }} do total de
            <strong class="total-data">{{ $data->total() }}</strong> <strong>{{ $data->count() > 1 ? 'resultados' : 'resultado' }}.</strong>
            @if(request()->getQueryString() != null)
                <strong class="text-warning"><a href="{{ request()->url() }}">Remover filtros</a></strong>
            @endif
        </small>
    @else
        <small class="text-muted p-4">
            <i class="material-icons">info</i>
            <strong>Ops!</strong> Nenhum foi registro encontrado.
            @if(request()->getQueryString() != null)
                <strong class="tetx-warning">
                    <a href="{{ request()->url() }}">Remover filtros</a>
                </strong>
            @endif
        </small>
    @endif
</nav>
