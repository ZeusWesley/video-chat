<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">

    <style>
        .bg {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: url('{{ asset('assets/img/bg.jpg') }}');
            background-size: 200px;
            opacity: .01;
        }
    </style>
</head>
<body>
<div class="bg"></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <p class="text-center">
                <img src="{{ asset('assets/img/AWRlogo.png') }}" width="140px" alt="All Woman Right" class="my-5">
            </p>

            <div class="card mt-5">
                <div class="card-header px-4 bg-primary text-center">
                    <small class="text-white">
                        Preencha os campos abaixo para acessar a sua área
                    </small>
                </div>

                <div class="card-body px-5">
                    @include('layouts.partial.messages')
                    <form method="POST" action="{{ url('/authenticate') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email">{{ __('Usuário') }}</label>
                            <input id="username" type="text" class="form-control" name="username"
                                   value="{{ old('username') }}" required autofocus>

                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">{{ __('Senha') }}</label>
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Acessar') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
