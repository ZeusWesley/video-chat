<form action="{{ url('messages/store') }}" class="post-data" data-callback="refresh">
    @csrf
    <div class="form-group">
        <label for="title">Título</label>
        <input type="text" name="title" id="title" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="comment">Comentário</label>
        <textarea type="text" name="comment" id="comment" class="form-control"></textarea>
    </div>

    <div class="card uploader text-center mb-3">
        <div class="btn-container">
            <!--the three icons: default, ok file (img), error file (not an img)-->
            <h1 class="imgupload"><i class="material-icons" style="font-size: 1em">video_library</i></h1>
            <h1 class="imgupload ok"><i class="material-icons" style="font-size: 1em">check_circle_outline</i></h1>
            <h1 class="imgupload stop"><i class="material-icons" style="font-size: 1em">close</i></h1>
            <!--this field changes dinamically displaying the filename we are trying to upload-->
            <p><small id="namefile">Somente vídeos (mp4)</small></p>
            <!--our custom btn which which stays under the actual one-->
            <button type="button" id="btnup" class="btn btn-primary btn-lg">Selecione o vídeo</button>
            <!--this is the actual file input, is set with opacity=0 beacause we wanna see our custom one-->
            <input type="file" value="" name="fileup" id="fileup">
        </div>
    </div>

    <button type="submit" class="btn btn-info btn-block">Salvar</button>
</form>
