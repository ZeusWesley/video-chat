<div class="row">
    @foreach($messages as $item)
        <div class="col-md-4 col-sm-6 col-lg-4">
            <div class="card mb-3">
                <div class="px-3 pt-3 bg-light bg-g-blue">
                    <div class="dropdown options-menu">

                        @authType(admin, teacher)
                        <button type="button" class="dropdown dropdown-toggle close options" id="dropdownMenuButton"
                                data-toggle="dropdown" data-dismiss="modal" aria-label="Close">
                            <i class="material-icons">gamepad</i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item text-info btn-show" href="#"
                               data-url="{{ url('users/all/'. $item->id) }}" title="Enviar vídeo para usuário">
                                <i class="material-icons">group_add</i>
                                Enviar para usuário</a>

                            <a class="dropdown-item text-info btn-show" href="#"
                               data-url="{{ url('messages/edit/'. $item->id) }}" title="Enviar vídeo para usuário">
                                <i class="material-icons">edit</i>
                                Editar</a>

                            <a class="dropdown-item text-danger btn-post" href="#"
                               data-action="{{ url('api/messages/delete/'.$item->id) }}" title="Deletar vídeo"
                               data-confirm-message="Tem certeza de que deseja deletar este material?" data-callback="refresh">
                                <i class="material-icons">delete</i>
                                Excluir</a>
                        </div>
                        @endAuthType
                    </div>
                    <h4 class="text-white py-5 text-center text-uppercase">{{ $item->title }}</h4>

                    <div class="play-video">
                        <i class="material-icons text-white">play_circle_filled</i>
                    </div>
                </div>

                <div class="card-body">
                    {{ strlen($item->comment) <= 100 ? $item->comment : substr($item->comment, 0, 50) . '...' }}
                </div>

                <div class="card-footer">
                    <p class="mb-0">
                        <small>Pessoas que podem ver:</small>
                    </p>
                    @foreach($item->clients as $user)
                        <span class="badge bg-gray round p-2">{{ $user->name }}</span>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>

<nav aria-label="...">
    <div>{{ $messages->links() }}</div>
    @if($messages->count() > 0 )
        <small class="text-muted">
            Listando <strong>{{ $messages->count() }}</strong>
            {{ $messages->count() > 1 ? 'registros' : 'registro' }} do total de
            <strong class="total-data">{{ $messages->total() }}</strong>
            <strong>{{ $messages->count() > 1 ? 'resultados' : 'resultado' }}.</strong>
            @if(request()->getQueryString() != null)
                <strong class="text-warning"><a href="{{ request()->url() }}">Remover filtros</a></strong>
            @endif
        </small>
    @else
        <small class="text-muted p-4">
            <i class="material-icons">info</i>
            <strong>Ops!</strong> Nenhum foi registro encontrado.
            @if(request()->getQueryString() != null)
                <strong class="tetx-warning">
                    <a href="{{ request()->url() }}">Remover filtros</a>
                </strong>
            @endif
        </small>
    @endif
</nav>
