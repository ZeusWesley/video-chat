@extends('layouts.app')

@php
    $pagename = 'Meus videos'
@endphp

@section('content')
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <h4>Lista</h4>
            </div>
            <div class="col text-right">
                <button class="btn btn-success btn-show"
                        title="Enviar novo vídeo"
                        data-url="{{ url('messages/create') }}">
                    Novo Material
                    <i class="material-icons">add</i>
                </button>
            </div>
        </div>

        <hr>
        @include('message.list')
    </div>
@endsection

@section('script')
    <script>
        function rel
    </script>
@endsection
