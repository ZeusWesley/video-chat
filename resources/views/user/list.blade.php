<table class="table table-striped align-middle">
    <tbody>
    @foreach($data as $item)

        {{--        @if($item->messages->count() >= 1)--}}
        {{--            {{ dd($item->messages) }}--}}
        {{--        @endif--}}
        <tr>
            <td class="align-middle">
                @if(!empty($item->photo))
                    <img src="{{ asset(storage_path($item->photo)) }}" alt="" class="rounded-circle">
                @else
                    <div class="rounded-circle bg-g-blue circle px-3 pt-2">
                        <strong class="text-white">{{ $item->name[0] }}</strong>
                    </div>
                @endif
            </td>
            <td class="align-middle">{{ $item->name }}</td>
            <td class="align-middle text-right">

                @if(!$item->hasMessage($message_id))
                    <button class="btn btn-rounded btn-success btn-post"
                            data-action="{{ url('api/users/' .$item->id. '/link/'. $message_id) }}"
                            data-callback="refresh">
                        <i class="material-icons">check</i>
                    </button>
                @else
                    <button class="btn btn-rounded btn-danger btn-post"
                            data-action="{{ url('api/users/' .$item->id. '/unlink/'. $message_id) }}"
                            data-callback="refresh">
                        <i class="material-icons">close</i>
                    </button>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
