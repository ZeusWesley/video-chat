@extends('layouts.app')

@php
    $pagename = 'Editar usuário';
@endphp

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Editar usuário</div>
                    <div class="card-body">
                        <form action="{{ url('users/update/'. $data->id) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">Nome</label>
                                <input type="text" name="name" id="name" class="form-control"
                                       value="{{ $data->name }}">
                            </div>
                            <div class="form-group">
                                <label for="username">Nome de usuário</label>
                                <input type="text" name="username" id="username" class="form-control"
                                       value="{{ $data->email }}">
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="text" name="email" id="email" class="form-control"
                                       value="{{ $data->email }}">
                            </div>
                            {{--                            <div class="form-group">--}}
                            {{--                                <label for="name">Nome de usuário</label>--}}
                            {{--                                <input type="text" name="name" id="name" class="form-control">--}}
                            {{--                            </div>--}}
                            <div class="form-group">
                                <label for="password">Senha</label>
                                <input type="text" name="password" id="password" class="form-control">
                            </div>

                            @if(\Illuminate\Support\Facades\Auth::user()->type == \App\Services\Data\UserType::SUPER)
                                <label for="type"></label>
                                <select name="type" id="type" class="form-control">
                                    @foreach(\App\Services\Data\UserType::$data as $key => $item)
                                        <option
                                            value="{{ $key }}" {{ $data->type == $key ? 'selected' : null }}>{{ $item['desc'] }}</option>
                                    @endforeach
                                </select>

                                <div class="form-group">
                                    <label for="active">Ativo</label>
                                    <input type="checkbox" name="active" id="active" class="form-control">
                                </div>
                            @endif

                            <div class="form-group">
                                <button class="btn btn-primary btn-block">
                                    Salvar Alterações
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
