@extends('layouts.app')

@php
    $pagename = $pageName;
@endphp

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Usuários
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>NOME</td>
                    <td>E-MAIL</td>
                    <td>USUÁRIO</td>
                    <td>TIPO</td>
                    <td>STATUS</td>
                    <td>AÇÕES</td>
                </tr>
                </thead>

                <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->username }}</td>
                        <td>{{ \App\Services\Data\UserType::$data[$item->type]['desc'] }}</td>
                        <td><span
                                class="badge {{ $item->active ? 'bg-primary' : 'bg-danger' }}">{{ $item->active ? 'Ativo' : 'Inativo' }}</span>
                        </td>
                        <td>
                            <a href="{{ url('users/edit/'. $item->id) }}" class="btn btn-primary">
                                <i class="material-icons">edit</i>
                            </a>
                            @if($item->type == \App\Services\Data\UserType::TEACHER)
                                <a href="{{ url('users/edit/'. $item->id) }}" class="btn btn-info"
                                   title="Material produzido">
                                    <i class="material-icons">ondemand_video</i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
