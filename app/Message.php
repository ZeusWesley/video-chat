<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['title', 'comment', 'path', 'active', 'created_by'];

    public function clients() {
        return $this->belongsToMany(User::class, 'user_has_messages', 'user_id', 'message_id');
    }
}
