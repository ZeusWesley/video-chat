<?php

namespace App\Providers;

use App\Services\Data\UserType;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::directive('authType', function ($expression) {
            $result = false;

            $expression = trim($expression);
            $expression = str_replace("'", '', $expression);
            $expression = explode(',', $expression);

            if (is_array($expression)) {
                foreach ($expression as $item)
                    if ($item == UserType::$data[Auth::user()->type]['key']) {
                        $result = true;
                    }
            } else
                if (UserType::$keys[$expression]['id'] == Auth::user()->type)
                    $result = true;

            return "<?php if ($result) { ?>";
        });

        Blade::directive('endAuthType', function () {
            return "<?php } ?>";
        });
    }
}
