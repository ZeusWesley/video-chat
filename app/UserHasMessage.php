<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasMessage extends Model
{
    protected $table = 'user_has_messages';
    protected $fillable = ['user_id', 'message_id', 'seen'];
}
