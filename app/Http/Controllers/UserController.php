<?php

namespace App\Http\Controllers;

use App\Services\Data\UserType;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index() {

        if(Auth::user()->type == UserType::SUPER)
            $pageName = 'Gestão de usuários';
        else
            $pageName = 'Clientes';
        $data = $this->service->index();
        return view('user.index', compact('data', 'pageName'));
    }

    public function create() {
        return view('user.create');
    }

    public function store(Request $request) {
        $data = $this->service->create($request->all());
        redirect('/users')->with('success', 'Usuário criado com sucesso!');
    }

    public function edit($id) {
        $data = $this->service->show($id);
        return view('user.edit', compact('data'));
    }

    public function all($message_id) {
        $data = $this->service->index();
        return view('user.list', compact('data', 'message_id'));
    }

    public function link($id, $message_id) {
        return $this->service->link($id, $message_id);
    }

    public function unlink($id, $message_id) {
        return $this->service->unlink($id, $message_id);
    }

    public function update($id, Request $request) {
        $data = $this->service->update($id, $request->all());
        return redirect('/users')->with('success', 'Usuário '. $data->name . ' atualizado com sucesso!');
    }
}


