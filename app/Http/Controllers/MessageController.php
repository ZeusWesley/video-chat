<?php

namespace App\Http\Controllers;

use App\Services\MessageService;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    protected $service;

    public function __construct(MessageService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request) {
        $messages = $this->service->index($request->all());
        return view('message.index', compact('messages'));
    }

    public function create() {
        return view('message.create');
    }

    public function store(Request $request) {
        $data = $this->service->create($request->all());
        return $data;
    }

    public function edit($id) {
        $data = $this->service->show($id);
        return view('message.edit', compact('data'));
    }

    public function update($id, Request $request) {
        $this->service->update($id, $request->all());
        return redirect('/messages')->with('success', 'Material atualizado com sucesso!');
    }

    public function delete($id) {
        return $this->service->delete($id);
    }
}


