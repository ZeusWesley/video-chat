<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Warehouse;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticate(Request $request)
    {
        $user = User::where('username', $request->get('username'))->first();

        if(empty($user))
            return redirect('/login')->with('error-message', 'Usuário não encontrado.');

        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            return redirect()->intended($this->redirectTo);
        } else {
            return redirect('/login')->with('error-message', 'Usuário/senha incorreto.');
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::forget('data');
        return redirect()->intended('login');
    }
}
