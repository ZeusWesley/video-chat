<?php


namespace App\Services;


use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class UploadService
{

    protected $mimes = ['mp4'];

    public function upload($data) {
        $path = 'uploads/'. Carbon::now()->day;

        $store = Storage::disk('public')->put($path, $data);

        return $store;
    }

}
