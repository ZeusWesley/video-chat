<?php


namespace App\Services\Data;


class UserType
{

    const SUPER = 0;
    const TEACHER = 1;
    const COMMON = 2;

    public static $data = [
        ['desc' => 'Super Administrador',   'color' => 'red',   'key' => 'admin'],
        ['desc' => 'Professor',             'color' => 'blue',  'key' => 'teacher'],
        ['desc' => 'Usuário Comum',         'color' => 'green', 'key' => 'common'],
    ];

    public static $keys = [
        'admin' =>      ['id' => 0, 'desc' => 'Super Administrador'],
        'teacher' =>    ['id' => 1, 'desc' => 'Professor'],
        'common' =>     ['id' => 2, 'desc' => 'Usuário Comum'],
    ];

}
