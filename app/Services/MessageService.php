<?php


namespace App\Services;


use App\Message;
use App\Services\Data\UserType;
use Illuminate\Support\Facades\Auth;
use MichaelDouglas\MService\MService;

class MessageService extends MService
{

    protected $model;
    protected $uploadService;
    protected $coverService;

    public function __construct(Message $model, UploadService $uploadService, CoverService $coverService)
    {
        $this->model = $model;
        $this->uploadService = $uploadService;
        $this->coverService = $coverService;
        parent::__construct();
    }

    public function create(array $data)
    {
        $file = $this->uploadService->upload($data['fileup']);

//        $cover = $this->coverService->getCover($file);
        $data['path'] = $file;
        $data['created_by'] = Auth::user()->id;

        return parent::create($data);
    }

    protected function customFilters($filters, &$query)
    {
        if(Auth::user()->type == UserType::TEACHER)
            $query->where('created_by', Auth::user()->id);

        parent::customFilters($filters, $query);
    }

    public function validators()
    {
        $this->validators = [
            'title' => 'required',
            'comment' => 'required',
            'path' => 'required',
        ];
    }

    public function updateValidators()
    {
        $this->validators = [];
    }

}
