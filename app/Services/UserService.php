<?php


namespace App\Services;


use App\User;
use MichaelDouglas\MService\MService;

class UserService extends MService
{

    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    public function update($id, array $data)
    {
        if (isset($data['password']) && empty($data['password']))
            unset($data['password']);
        else
            $data['password'] = bcrypt($data['password']);

        return parent::update($id, $data);
    }

    public function link($id, $message_id) {
        $user = $this->show($id);

        return $user->messages()->sync($message_id);
    }

    public function unlink($id, $message_id) {
        $user = $this->show($id);

        return $user->messages()->detach($message_id);
    }

    public function validators()
    {
        $this->validators = [];
    }

    public function updateValidators()
    {
        $this->validators = [];
    }

}
