<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Darth Vader',
                'username' => 'superadmin',
                'email' => 'eliseu.wp59@gmail.com',
                'password' => bcrypt('admin@123'),
                'type' => 0,
            ],
            [
                'name' => 'Mestre Jedi',
                'username' => 'mestrejedi',
                'email' => 'mestrejedi@gmail.com',
                'password' => bcrypt('admin@123'),
                'type' => 2,
            ],
            [
                'name' => 'Aprendiz de Jedi',
                'username' => 'aprendizjedi',
                'email' => 'aprendizjedi@gmail.com',
                'password' => bcrypt('admin@123'),
                'type' => 2,
            ],
            [
                'name' => 'Luke',
                'username' => 'luke',
                'email' => 'luke@gmail.com',
                'password' => bcrypt('admin@123'),
                'type' => 1,
            ],
            [
                'name' => 'Precioso',
                'username' => 'precioso',
                'email' => 'precioso@gmail.com',
                'password' => bcrypt('admin@123'),
                'type' => 1,
            ],
        ];

        foreach($users as $user) {
            DB::table('users')->insert($user);
        }
    }
}
