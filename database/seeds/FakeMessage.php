<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FakeMessage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            'title' => 'Video Fake',
            'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'path' => 'public/videos/1.png',
            'created_by' => 2
        ]);
        DB::table('user_has_messages')->insert([
            'user_id' => 3,
            'message_id' => 1,
            'seen' => 0,
        ]);
    }
}
