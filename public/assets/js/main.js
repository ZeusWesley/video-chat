$(document).on('click', '.btn-show', function(e) {
    e.preventDefault();

    var url = $(this).attr('data-url');
    var title = $(this).attr('title');

    $.get(url).done((r) => {
        $('#modalshow .modal-body').html(r);
        $('#modalshow .modal-title').html(title);
        $('#modalshow').modal('show');
    }).fail((e) => {
        alert(e.responseJSON.error);
    });
});

$(document).on('submit', '.post-data', function(e) {
    e.preventDefault();

    var url = $(this).attr('action');
    var formData = new FormData($(this)[0]);
    var msg = $(this).attr('data-confirm-message');
    var callback = $(this).attr('data-callback');

    if(msg) {
        confirmAlert(msg, function () {
            postRequest(url, formData, callback);
        })
    } else {
        postRequest(url, formData, callback);
    }
});

$(document).on('click', '.btn-post', function(e) {
    e.preventDefault();

    var url = $(this).attr('data-action');
    var msg = $(this).attr('data-confirm-message');
    var callback = $(this).attr('data-callback');

    if(msg) {
        confirmAlert(msg, function () {
            postRequest(url, null, callback);
        })
    } else {
        postRequest(url, null, callback);
    }
});

function postRequest(url, formData, callback) {
    Swal.showLoading();
    $.ajax({
        type: "POST",
        url: url,
        data: formData,
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR) {
            successAlert('Sucesso!', 'Ação executada com sucesso!', function () {
                if (callback)
                    window[callback]();
            });
            $('#modal-show').modal('hide');
        },
        error: function(e, textStatus, jqXHR) {
            var msg = e.error ? e.error : e.responseJSON.message;

            errorAlert(msg);
        },
    });
}

$(document).on('change', '#fileup', function(){
//here we take the file extension and set an array of valid extensions
    var res=$('#fileup').val();
    var arr = res.split("\\");
    var filename=arr.slice(-1)[0];
    filextension=filename.split(".");
    filext="."+filextension.slice(-1)[0];
    valid=[".mp4"];
//if file is not valid we show the error icon, the red alert, and hide the submit button
    if (valid.indexOf(filext.toLowerCase())==-1){
        $( ".imgupload" ).hide(200);
        $( ".imgupload.ok" ).hide(200);
        $( ".imgupload.stop" ).show(200);

        $('#namefile').css({"color":"red","font-weight":700});
        $('#namefile').html("File "+filename+" is not  pic!");

        $( "#submitbtn" ).hide();
        $( "#fakebtn" ).show();
    }else{
        //if file is valid we show the green alert and show the valid submit
        $( ".imgupload" ).hide(200);
        $( ".imgupload.stop" ).hide(200);
        $( ".imgupload.ok" ).show(200);

        $('#namefile').css({"color":"green","font-weight":700});
        $('#namefile').html(filename);

        $( "#submitbtn" ).show();
        $( "#fakebtn" ).hide();
    }
});

function refresh() {
    location.reload();
}
