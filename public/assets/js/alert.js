function errorAlert(message, callback) {
    Swal.fire("Erro...", message, "error").then(function () {
        if (callback)
            callback();
        else if (message == 'Sua sessão expirou.')
            window.location = baseUrl;
    });
}

function successAlert(title, text, callback) {
    Swal.fire({
        title: title,
        text: text,
        type: 'success',
        icon: 'success',
        allowOutsideClick: false
    }).then(function () {
        callback();
    });
}

function confirmAlert(text, callback) {
    Swal.fire({
        title: 'Atenção!',
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#61d6b2',
        cancelButtonColor: '#b7bfca',
        confirmButtonText: 'Sim',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            callback();
        }
    })
}

function alertKill() {
    Swal.close();
}
