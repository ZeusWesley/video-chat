<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/authenticate', '\App\Http\Controllers\Auth\LoginController@authenticate');


Route::middleware(['auth'])->group(function () {
    Route::get('/users', 'UserController@index');
    Route::get('/users/edit/{id}', 'UserController@edit');
    Route::post('/users/update/{id}', 'UserController@update');
    Route::post('/users/create', 'UserController@create');
    Route::get('/users/all/{message_id}', 'UserController@all');

    Route::get('/users/messages/{id}', 'MessageController@byId');

    Route::get('/messages', 'MessageController@index');
    Route::get('/messages/edit/{id}', 'MessageController@edit');
    Route::post('/messages/update/{id}', 'MessageController@update');
    Route::get('/messages/create', 'MessageController@create');
    Route::post('/messages/store', 'MessageController@store');
});
