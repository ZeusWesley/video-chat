<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/users/{id}/link/{message_id}', 'UserController@link');
Route::post('/users/{id}/unlink/{message_id}', 'UserController@unlink');

Route::post('/messages/delete/{id}', 'MessageController@delete');
